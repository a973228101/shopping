<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicons -->
    <link rel="shortcut icon" href="{{ asset('images/Mars/mars.ico') }}" type="image/x-icon">
    <!-- Title -->
    <title>火星鐘錶</title>
    <!-- ************************* CSS Files ************************* -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <!-- Elegant Icon CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@icon/elegant-icons@0.0.1-alpha.4/elegant-icons.css">
    <!-- All Plugins CSS css -->
    <link rel="stylesheet" href="{{ asset('css/plugins.css') }}">
    <!-- style css -->
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <!-- modernizr JS
    	============================================ -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" integrity="sha512-3n19xznO0ubPpSwYCRRBgHh63DrV+bdZfHK52b1esvId4GsfwStQNPJFjeQos2h3JwCmZl0/LgLxSKMAI55hgw==" crossorigin="anonymous"></script>
</head>

<body>
    <!-- Main Wrapper Start -->
    <div class="wrapper bg--shaft">
        <!-- Header Area Start -->
        <header class="header headery-style-1">
            <div class="header-middle header-top-1">
                <div class="container">
                    <div class="row no-gutters align-items-center">
                        <div class="col-md-4 order-lg-1 order-2 d-none d-sm-none d-md-block">
                            <div class="contact-info" style="display: none;">
                                <img src="{{ asset('images/Mars/icon_phone.png') }}" alt="Phone Icon">
                                <p>聯絡電話 <br>(02)2541-3755</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-8 col-12 order-lg-2 order-1 text-center">
                            <a href="index.php" class="logo-box">
                                <img src="{{ asset('images/Mars/logo.png') }}" alt="logo">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-bottom header-top-1 position-relative navigation-wrap fixed-header">
                <div class="container position-static">
                    <div class="row">
                        <div class="col-8 text-left">
                            <nav class="main-navigation">
                                <ul class="mainmenu">
                                    <li class="mainmenu__item active">
                                        <a href="index.php" class="mainmenu__link">首頁</a>
                                    </li>
                                    <li class="mainmenu__item ">
                                        <a href="index.php?do=news" class="mainmenu__link">最新消息</a>
                                    </li>
                                    <li class="mainmenu__item ">
                                        <a href="index.php?do=information" class="mainmenu__link">交易說明</a>
                                    </li>
                                    <li class="mainmenu__item ">
                                        <a href="index.php?do=contact" class="mainmenu__link">聯絡我們</a>
                                    </li>
                                    <li class="mainmenu__item ">
                                        <a href="index.php?do=about" class="mainmenu__link mr-5">關於火星</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-4 header-toolbar-custom">
                            <div class="header-toolbar">
                                <ul class="header-toolbar-icons">
                                    <!--  login add   -->
                                    @if (Route::has('login'))
                                    @auth
                                    <li class="mainmenu__item "> <a href="{{ url('/dashboard') }}" class="text-sm text-gray-700 underline">Dashboard</a> </li>
                                    @else
                                    <li class="mainmenu__item "><a href="{{ route('login') }}">登入</a> </li>

                                    @if (Route::has('register'))
                                    <li class="mainmenu__item "><a href="{{ route('register') }}">註冊</a> </li>
                                    @endif
                                    @endauth
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                    <script>
                        $("#myfirstsearchform").keydown(function(e) {
                            var e = e || event,
                                keycode = e.which || e.keyCode;
                            if (keycode == 13) {
                                $(".myfirstbtn").trigger("click");
                            }
                        });

                        function kwserch() {
                            //alert('aaa');
                            kw = $('input[name=kw]').val();
                            window.location.href = 'index.php?do=search&kw=' + kw;
                        }
                    </script>
                    <div class="row no-gutters">
                        <div class="col-12">
                            <div class="mobile-menu">
                                <div class="mobile-contentmenu">
                                    <div class="btn-group" role="group" aria-label="商品分類">
                                        <a role="button" class="btn" href="index.php?do=home">發燒新貨</a>
                                        <a role="button" class="btn" href="index.php?do=category&path=2">精品特賣</a>
                                        <a role="button" class="btn" href="index.php?do=category&path=3">名品珠寶</a>
                                    </div>

                                    <div class="search-mobile--group">
                                        <form action="index.php" id="mysecondsearchform" method="get">
                                            <div>
                                                <div class="input-group">
                                                    <input type="hidden" name="do" value="search">
                                                    <input type="text" name="kw" class="form-control" placeholder="搜尋關鍵字" value="">
                                                    <div class="input-group-append">
                                                        <button type="submit" id="mysecondbtn"><i class="fa fa-search"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <script>
                                            $("#mysecondsearchform").keydown(function(e) {
                                                var e = e || event,
                                                    keycode = e.which || e.keyCode;
                                                if (keycode == 13) {
                                                    $(".mysecondbtn").trigger("click");
                                                }
                                            });

                                            function changetype() {
                                                tpid = $('select[name=tpid]').val();
                                                window.location.href = 'index.php?do=search&tpid=' + tpid;
                                            }

                                            function kwserch2() {
                                                //alert('aaa');
                                                tpid = $('select[name=tpid]').val();
                                                kw = $('input[name=kw2]').val();
                                                window.location.href = 'index.php?do=search&kw=' + kw;
                                            }
                                        </script>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Header Area End -->
        <!--內容顯示：開始-->
        <!-- Breadcumb area Start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="banner">
                            <img src="{{ asset('images/Mars/img-middle-mirora1.jpg') }}" class="img-responsive">
                        </div>
                        <ul class="breadcrumb justify-content-end">
                            <li><a href="index.php"><i class="fa fa-home"></i> 首頁</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Breadcumb area End -->
        <!-- Main Content Wrapper Start -->
        <div class="main-content-wrapper">
            <div class="shop-area pt--40 pb--80 pt-md--30 pb-md--30">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-9 order-lg-2 mb-md--30">
                            <div class="row">
                                <div class="col-12">
                                    @yield('watch')
                                </div>

                                <!--頁面左邊類別顯示：-->
                                <div class="col-lg-3 order-lg-1">
                                    <aside class="shop-sidebar">
                                        <div class="search-filter d-none d-sm-none d-md-none d-lg-block">
                                            <div class="category-layered">
                                                <h3 class="category-heading">商品分類</h3>
                                                <ul class="category-list">
                                                    <li class="current"><a href="index.php?do=home">發燒新貨</a></li>
                                                    <li><a href="index.php?do=category&path=2">精品特賣 暫無</a></li>
                                                    <li><a href="index.php?do=category&path=3">名品珠寶 暫無</a></li>
                                                </ul>
                                            </div>

                                            <div class="category-layered">
                                                <div class="category-top">
                                                    <h3 class="category-heading">各大品牌</h3>

                                                </div>
                                                <ul class="category-list" id="pinpaixianshi">
                                                    <li><a href="index.php?do=category&path=18">Audemars Piguet【愛彼】</a></li>
                                                    <li><a href="index.php?do=category&path=6">Blancpain【寶鉑】</a></li>
                                                    <li><a href="index.php?do=category&path=19">Breguet【寶璣】</a></li>
                                                    <li><a href="index.php?do=category&path=20">Bvlgari【寶格麗】</a></li>
                                                    <li><a href="index.php?do=category&path=21">Cartier【卡地亞】</a></li>
                                                    <li><a href="index.php?do=category&path=23">Chanel【香奈兒】</a></li>
                                                    <li><a href="index.php?do=category&path=22">Chopard【蕭邦】</a></li>
                                                </ul>
                                            </div>

                                            <div class="category-layered">
                                                <h3 class="category-heading">其它</h3>
                                                <ul class="category-list">
                                                    <!--<li><a href="#">精品配件</a></li>-->
                                                    <li><a href="index.php?do=category&path=82">其他商品</a></li>
                                                    <li><a href="index.php?do=category&path=119">原廠面盤</a></li>
                                                    <li><a href="index.php?do=category&path=108">精緻錶盒</a></li>
                                                </ul>
                                            </div>

                                        </div>

                                        <div class="social-links justify-content-center mb--30">
                                            <a href="https://line.me/ti/p/DzBgXkgXbR" target="_blank" style="cursor: unset;">
                                                <img src="{{ asset('images/Mars/qrcode-line.jpg') }}" alt="line" class="img-responsive">
                                                <span>Line ID:Mars1234567</span>
                                            </a>
                                        </div>
                                        <div class="social-links justify-content-center mb--15">
                                            <a href="javascript:void(0);" style="cursor: unset;">
                                                <img src="{{ asset('images/Mars/qrcode-wexin.jpg') }}" alt="weixin" class="img-responsive">
                                                <span>微信ID: Mars1234567</span>
                                            </a>
                                        </div>
                                        <div class="social-links justify-content-center">
                                            <a href="tel:02-2541-3755">
                                                <span class="phone"><i class="fa fa-phone"></i> (03) 8888-8888</span>
                                            </a>
                                        </div>

                                    </aside>
                                </div>
                                <script>
                                    function getItems(ppid) {
                                        //alert('aaa');
                                        //$('select[name=tpid]').empty();
                                        $.ajax({
                                            type: "GET",
                                            url: "ajaxAction.php",
                                            data: "action=getItems&ppid=" + ppid,
                                            success: function(items) {
                                                $('#pinpaixianshi').empty();
                                                $('#pinpaixianshi').append(items);
                                            }
                                        });
                                    }
                                </script>
                                <!--頁面左邊類別顯示：-->


                            </div>
                        </div>
                    </div>
                </div>
                <!-- Main Content Wrapper Start -->
                <!--內容顯示：結束-->

                <!-- Footer Start -->
                <footer class="footer border-top ptb--40 ptb-md--30">
                    <div class="container">
                        <div class="row">

                            <div class="col-sm-8 col-xs-12 offset-md-5">
                                <p>地址：桃園市楊梅區學成路11號</p>
                                <p class="copyright-text">copyright &copy; 2020 <a href="#">MARS STAR - 火星鐘錶</a>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- Footer End -->

                <!-- Scroll To Top Start -->
                <a class="scroll-to-top" href=""><i class="fa fa-angle-up"></i></a>
                <!-- Scroll To Top End -->

            </div>
            <!-- Main Wrapper End -->


            <!-- ************************* JS Files ************************* -->

            <!-- jQuery JS -->
            <script src="{{ asset('js/jquery.min.js') }}"></script>

            <!-- Bootstrap and Popper Bundle JS -->
            <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

            <!-- All Plugins Js -->
            <script src="{{ asset('js/plugins.js') }}"></script>
            <!-- Ajax Mail Js -->
            <script src="{{ asset('js/ajax-mail.js') }}"></script>
            <!-- Main JS -->
            <script src="{{ asset('js/main.js') }}"></script>

            <script type="text/javascript">
                $(document).ready(function() {
                    $('.meanmenu-extend').click(function() {
                        $('.mobile-contentmenu').toggle();
                    });
                });
            </script>
</body>

</html>
