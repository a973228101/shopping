@extends('layouts._board')


@section('watch')
<!-- Shop Toolbar Start -->
<div class="shop-toolbar">
	<h1 class="page-title">發燒新貨</h1>
	<div class="product-view-mode" data-default="3">
		<a class="grid-2" data-target="gridview-2" data-toggle="tooltip" data-placement="top" title="2">2</a>
		<a class="active grid-3" data-target="gridview-3" data-toggle="tooltip" data-placement="top" title="3">3</a>
		<a class="grid-4" data-target="gridview-4" data-toggle="tooltip" data-placement="top" title="4">4</a>
		<a class="grid-5" data-target="gridview-5" data-toggle="tooltip" data-placement="top" title="5">5</a>
		<a class="list" data-target="listview" data-toggle="tooltip" data-placement="top" title="5">List</a>
	</div>
	<span class="product-pages">總筆數 12</span>
</div>
<!-- Shop Toolbar End -->
</div>
</div>

<div class="col-12 m-0 p-0">
<div class="embed-responsive embed-responsive-16by9">
<video src="{{ asset('video/watch.mp4/') }}" autoplay loop muted class="embed-responsive-item"></video>
</div>
</div>

<!-- Main Shop wrapper Start -->
<div class="shop-product-wrap grid gridview-3 row no-gutters">
<!--產品列表顯示開始：-->

<div class="col-xl-3 col-lg-4 col-md-6 col-12">
<div class="mirora-product mb-md--10">
	<div class="product-img">
		<a href="index.php?products=6900">
			<img src="{{ asset('images/Mars/Grand Seiko.jpg') }}" alt="Grand Seiko錶" class="primary-image" />
			<img src="{{ asset('images/Mars/Grand Seiko2.jpg') }}" alt="Grand Seiko錶" class="secondary-image">
		</a>

		<div class="product-img-overlay" onClick="location.href='index.php?products=6900'">
			<span class="product-tags">NEW</span>
		</div>

	</div>
	<div class="product-content text-center">
		<span>其他品牌</span>
		<h4><a href="index.php?products=6900">Grand Seiko錶</a></h4>
		<div class="product-price-wrapper">
			<span class="money"></span>
		</div>
	</div>
	<div class="mirora_product_action text-center position-absolute">
		<div class="product-action">
			<a class="add_cart cart-item action-cart" href="index.php?do=contact" title="我要洽談"><span>我要洽談</span></a>
			<a class="add_cart cart-item action-cart" href="index.php?products=6900" title="查看詳情"><span>查看詳情</span></a>
		</div>
	</div>
</div>
<div class="mirora-product-list">
	<div class="product-img">
		<a href="index.php?products=6900">
			<img src="{{ asset('images/Mars/Grand Seiko.jpg') }}" alt="Grand Seiko錶" class="primary-image" />
			<img src="{{ asset('images/Mars/Grand Seiko.jpg2') }}" alt="Grand Seiko錶" class="secondary-image">
		</a>
		<div class="product-img-overlay" onClick="location.href='index.php?products=6900'">
			<span class="product-tags">NEW</span>
		</div>
	</div>
	<div class="product-content">
		<span>原廠面盤</span>
		<h4><a href="index.php?products=6900">各大品牌原廠面盤</a></h4>
		<div class="product-price-wrapper">
			<span class="money"></span>
		</div>
		<div class="product-action">
			<a class="add_cart cart-item action-cart" href="index.php?products=6900" title="查看詳情"><span>查看詳情</span></a>
			<a class="add_cart cart-item action-cart" href="index.php?do=contact" title="我要洽談"><span>我要洽談</span></a>
		</div>
	</div>
</div>
</div><div class="col-xl-3 col-lg-4 col-md-6 col-12">
<div class="mirora-product mb-md--10">
	<div class="product-img">
		<a href="index.php?products=6900">
			<img src="{{ asset('images/Mars/蕭邦錶.jpg') }}" alt="蕭邦錶" class="primary-image" />
			<img src="{{ asset('images/Mars/蕭邦錶2.jpg') }}" alt="蕭邦錶" class="secondary-image">
		</a>

		<div class="product-img-overlay" onClick="location.href='index.php?products=6900'">
			<span class="product-tags">NEW</span>
		</div>

	</div>
	<div class="product-content text-center">
		<span>原廠面盤</span>
		<h4><a href="index.php?products=6900">各大品牌原廠面盤</a></h4>
		<div class="product-price-wrapper">
			<span class="money"></span>
		</div>
	</div>
	<div class="mirora_product_action text-center position-absolute">
		<div class="product-action">
			<a class="add_cart cart-item action-cart" href="index.php?do=contact" title="我要洽談"><span>我要洽談</span></a>
			<a class="add_cart cart-item action-cart" href="index.php?products=6900" title="查看詳情"><span>查看詳情</span></a>
		</div>
	</div>
</div>
<div class="mirora-product-list">
	<div class="product-img">
		<a href="index.php?products=6900">
			<img src="{{ asset('images/Mars/蕭邦錶.jpg') }}" alt="蕭邦錶" class="primary-image" />
			<img src="{{ asset('images/Mars/蕭邦錶2.jpg') }}}" alt="蕭邦錶" class="secondary-image">
		</a>
		<div class="product-img-overlay" onClick="location.href='index.php?products=6900'">
			<span class="product-tags">NEW</span>
		</div>
	</div>
	<div class="product-content">
		<span>原廠面盤</span>
		<h4><a href="index.php?products=6900">各大品牌原廠面盤</a></h4>
		<div class="product-price-wrapper">
			<span class="money"></span>
		</div>
		<div class="product-action">
			<a class="add_cart cart-item action-cart" href="index.php?products=6900" title="查看詳情"><span>查看詳情</span></a>
			<a class="add_cart cart-item action-cart" href="index.php?do=contact" title="我要洽談"><span>我要洽談</span></a>
		</div>
	</div>
</div>
</div><div class="col-xl-3 col-lg-4 col-md-6 col-12">
<div class="mirora-product mb-md--10">
	<div class="product-img">
		<a href="index.php?products=6900">
			<img src="{{ asset('images/Mars/帝舵錶.jpg') }}" alt="帝舵錶" class="primary-image" />
			<img src="{{ asset('images/Mars/帝舵錶2.jpg') }}" alt="帝舵錶" class="secondary-image">
		</a>

		<div class="product-img-overlay" onClick="location.href='index.php?products=6900'">
			<span class="product-tags">NEW</span>
		</div>

	</div>
	<div class="product-content text-center">
		<span><!--Promesse 系列-->原廠面盤</span>
		<h4><a href="index.php?products=6900"><!--愛彼錶-->各大品牌原廠面盤</a></h4>
		<div class="product-price-wrapper">
			<span class="money"><!--$170000--></span>
		</div>
	</div>
	<div class="mirora_product_action text-center position-absolute">
		<div class="product-action">
			<a class="add_cart cart-item action-cart" href="index.php?do=contact" title="我要洽談"><span>我要洽談</span></a>
			<a class="add_cart cart-item action-cart" href="index.php?products=6900" title="查看詳情"><span>查看詳情</span></a>
		</div>
	</div>
</div>
<div class="mirora-product-list">
	<div class="product-img">
		<a href="index.php?products=6900">
			<img src="{{ asset('images/Mars/帝舵錶.jpg') }}" alt="帝舵錶" class="primary-image" />
			<img src="{{ asset('images/Mars/帝舵錶2.jpg') }}" alt="帝舵錶" class="secondary-image">
		</a>
		<div class="product-img-overlay" onClick="location.href='index.php?products=6900'">
			<span class="product-tags">NEW</span>
		</div>
	</div>
	<div class="product-content">
		<span>原廠面盤</span>
		<h4><a href="index.php?products=6900">各大品牌原廠面盤</a></h4>
		<div class="product-price-wrapper">
			<span class="money"></span>
		</div>
		<div class="product-action">
			<a class="add_cart cart-item action-cart" href="index.php?products=6900" title="查看詳情"><span>查看詳情</span></a>
			<a class="add_cart cart-item action-cart" href="index.php?do=contact" title="我要洽談"><span>我要洽談</span></a>
		</div>
	</div>
</div>
</div><div class="col-xl-3 col-lg-4 col-md-6 col-12">
<div class="mirora-product mb-md--10">
	<div class="product-img">
		<a href="index.php?products=6900">
			<img src="{{ asset('images/Mars/法蘭克穆勒錶.jpg') }}" alt="法蘭克穆勒錶" class="primary-image" />
			<img src="{{ asset('images/Mars/法蘭克穆勒錶2.jpg') }}" alt="法蘭克穆勒錶" class="secondary-image">
		</a>

		<div class="product-img-overlay" onClick="location.href='index.php?products=6900'">
			<span class="product-tags">NEW</span>
		</div>

	</div>
	<div class="product-content text-center">
		<span><!--Promesse 系列-->原廠面盤</span>
		<h4><a href="index.php?products=6900"><!--愛彼錶-->各大品牌原廠面盤</a></h4>
		<div class="product-price-wrapper">
			<span class="money"><!--$170000--></span>
		</div>
	</div>
	<div class="mirora_product_action text-center position-absolute">
		<div class="product-action">
			<a class="add_cart cart-item action-cart" href="index.php?do=contact" title="我要洽談"><span>我要洽談</span></a>
			<a class="add_cart cart-item action-cart" href="index.php?products=6900" title="查看詳情"><span>查看詳情</span></a>
		</div>
	</div>
</div>
<div class="mirora-product-list">
	<div class="product-img">
		<a href="index.php?products=6900">
			<img src="{{ asset('images/Mars/法蘭克穆勒錶.jpg') }}" alt="法蘭克穆勒錶" class="primary-image" />
			<img src="{{ asset('images/Mars/法蘭克穆勒錶2.jpg') }}" alt="法蘭克穆勒錶" class="secondary-image">
		</a>
		<div class="product-img-overlay" onClick="location.href='index.php?products=6900'">
			<span class="product-tags">NEW</span>
		</div>
	</div>
	<div class="product-content">
		<span><!--Promesse 系列-->原廠面盤</span>
		<h4><a href="index.php?products=6900"><!--愛彼錶-->各大品牌原廠面盤</a></h4>
		<div class="product-price-wrapper">
			<span class="money"><!--$170000--></span>
		</div>
		<div class="product-action">
			<a class="add_cart cart-item action-cart" href="index.php?products=6900" title="查看詳情"><span>查看詳情</span></a>
			<a class="add_cart cart-item action-cart" href="index.php?do=contact" title="我要洽談"><span>我要洽談</span></a>
		</div>
	</div>
</div>
</div><div class="col-xl-3 col-lg-4 col-md-6 col-12">
<div class="mirora-product mb-md--10">
	<div class="product-img">
		<a href="index.php?products=6900">
			<img src="{{ asset('images/Mars/萬國錶.jpg') }}" alt="萬國錶" class="primary-image" />
			<img src="{{ asset('images/Mars/萬國錶2.jpg') }}" alt="萬國錶" class="secondary-image">
		</a>

		<div class="product-img-overlay" onClick="location.href='index.php?products=6900'">
			<span class="product-tags">NEW</span>
		</div>

	</div>
	<div class="product-content text-center">
		<span><!--Promesse 系列-->原廠面盤</span>
		<h4><a href="index.php?products=6900"><!--愛彼錶-->各大品牌原廠面盤</a></h4>
		<div class="product-price-wrapper">
			<span class="money"><!--$170000--></span>
		</div>
	</div>
	<div class="mirora_product_action text-center position-absolute">
		<div class="product-action">
			<a class="add_cart cart-item action-cart" href="index.php?do=contact" title="我要洽談"><span>我要洽談</span></a>
			<a class="add_cart cart-item action-cart" href="index.php?products=6900" title="查看詳情"><span>查看詳情</span></a>
		</div>
	</div>
</div>
<div class="mirora-product-list">
	<div class="product-img">
		<a href="index.php?products=6900">
			<img src="{{ asset('images/Mars/萬國錶.jpg') }}}" alt="萬國錶" class="primary-image" />
			<img src="{{ asset('images/Mars/萬國錶2.jpg') }}" alt="萬國錶" class="secondary-image">
		</a>
		<div class="product-img-overlay" onClick="location.href='index.php?products=6900'">
			<span class="product-tags">NEW</span>
		</div>
	</div>
	<div class="product-content">
		<span><!--Promesse 系列-->原廠面盤</span>
		<h4><a href="index.php?products=6900"><!--愛彼錶-->各大品牌原廠面盤</a></h4>
		<div class="product-price-wrapper">
			<span class="money"><!--$170000--></span>
		</div>
		<div class="product-action">
			<a class="add_cart cart-item action-cart" href="index.php?products=6900" title="查看詳情"><span>查看詳情</span></a>
			<a class="add_cart cart-item action-cart" href="index.php?do=contact" title="我要洽談"><span>我要洽談</span></a>
		</div>
	</div>
</div>
</div><div class="col-xl-3 col-lg-4 col-md-6 col-12">
<div class="mirora-product mb-md--10">
	<div class="product-img">
		<a href="index.php?products=6900">
			<img src="{{ asset('images/Mars/伯爵錶.jpg') }}" alt="伯爵錶" class="primary-image" />
			<img src="{{ asset('images/Mars/伯爵錶2.jpg') }}" alt="伯爵錶" class="secondary-image">
		</a>

		<div class="product-img-overlay" onClick="location.href='index.php?products=6900'">
			<span class="product-tags">NEW</span>
		</div>

	</div>
	<div class="product-content text-center">
		<span><!--Promesse 系列-->原廠面盤</span>
		<h4><a href="index.php?products=6900"><!--愛彼錶-->各大品牌原廠面盤</a></h4>
		<div class="product-price-wrapper">
			<span class="money"><!--$170000--></span>
		</div>
	</div>
	<div class="mirora_product_action text-center position-absolute">
		<div class="product-action">
			<a class="add_cart cart-item action-cart" href="index.php?do=contact" title="我要洽談"><span>我要洽談</span></a>
			<a class="add_cart cart-item action-cart" href="index.php?products=6900" title="查看詳情"><span>查看詳情</span></a>
		</div>
	</div>
</div>
<div class="mirora-product-list">
	<div class="product-img">
		<a href="index.php?products=6900">
			<img src="{{ asset('images/Mars/伯爵錶.jpg') }}" alt="伯爵錶" class="primary-image" />
			<img src="{{ asset('images/Mars/伯爵錶2.jpg') }}" alt="伯爵錶" class="secondary-image">
		</a>
		<div class="product-img-overlay" onClick="location.href='index.php?products=6900'">
			<span class="product-tags">NEW</span>
		</div>
	</div>
	<div class="product-content">
		<span><!--Promesse 系列-->原廠面盤</span>
		<h4><a href="index.php?products=6900"><!--愛彼錶-->各大品牌原廠面盤</a></h4>
		<div class="product-price-wrapper">
			<span class="money"><!--$170000--></span>
		</div>
		<div class="product-action">
			<a class="add_cart cart-item action-cart" href="index.php?products=6900" title="查看詳情"><span>查看詳情</span></a>
			<a class="add_cart cart-item action-cart" href="index.php?do=contact" title="我要洽談"><span>我要洽談</span></a>
		</div>
	</div>
</div>
</div>
<!--產品列表顯示結束：-->
</div>
<!-- Main Shop wrapper End -->
<!-- Pagination Start -->
<div class="pagination-wrap mt--15 mt-md--10">
<p class="page-ammount">總筆數 6</p>
</div>
<!-- Pagination End -->



@stop
