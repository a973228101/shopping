<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Product extends Model
{
    use HasFactory;
    protected $table = 'products';
    protected $fillable = ['title', 'pic', 'price', 'desc', 'sell_at', 'enabled', 'cgy_id', 'source', 'options'];

    public function cgy()
    {
        return $this->BelongsTo('\App\Models\Cgy');
    }
}
